package com.featureData.main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.featureData.main.controllers.OverAllResultController;
import com.featureData.main.entities.FeatureLine;
import com.featureData.main.entities.OverAllResult;
import com.featureData.main.repositories.FeatureLineRepository;
import com.featureData.main.repositories.OverAllResultRepository;

@SpringBootApplication
public class SeleTrackApplication implements CommandLineRunner{

	@Autowired
	private OverAllResultController overAllResultController;
	
	@Autowired
	private FeatureLineRepository featureLineRepository;
	
	@Autowired
	private OverAllResultRepository overAllResultRepository;
	
	Logger logger = LoggerFactory.getLogger(SeleTrackApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(SeleTrackApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//	 	clearData();
//    	saveData();
//    	showData();
	}

   @Transactional
    private void clearData(){
	   featureLineRepository.deleteAll();
	   overAllResultRepository.deleteAll();
    }
    
    @Transactional
    private void saveData(){
    	saveDataToDB();
    }
    
    @Transactional
    private void showData(){
    	List<FeatureLine> featureLineList = featureLineRepository.findAll();
        List<OverAllResult> overAllResultList = overAllResultRepository.findAll();
         
        logger.info("===================FeatureLine List:==================");
        featureLineList.forEach(System.out::println);
         
        logger.info("===================OverAllResult List:==================");
        overAllResultList.forEach(System.out::println);
    }
    
    private void saveDataToDB(){
    	OverAllResult overAllResult = new OverAllResult();
    	overAllResult.setBrowser("Chrome");
    	overAllResult.setStatus("NotRun");
    	overAllResult.setTesesetid(new Long(87));
    	List<FeatureLine> featureLineSet = new ArrayList<>();
    	FeatureLine featureLine = new FeatureLine("StepNumber1", "actual_result1", "status1", "description1", "featureline1", null, null, null, overAllResult);
    	featureLineSet.add(featureLine);
    	featureLine = new FeatureLine("StepNumber2", "actual_result2", "status2", "description2","featureline2", null, null, null, overAllResult);
    	featureLineSet.add(featureLine);
    	overAllResult.setFeatureLines(featureLineSet);
    	overAllResultController.createOverAllResult(overAllResult);
		
    	overAllResult = new OverAllResult();
    	overAllResult.setBrowser("Firefox");
    	overAllResult.setStatus("Run");
    	overAllResult.setTesesetid(new Long(88));
    	featureLineSet = new ArrayList<>();
    	featureLine = new FeatureLine("StepNumber3", "actual_result3", "status3", "description3", "featureline3", null, null, null, overAllResult);
    	featureLineSet.add(featureLine);
    	featureLine = new FeatureLine("StepNumber4", "actual_result4", "status4", "description4","featureline4", null, null, null, overAllResult);
    	featureLineSet.add(featureLine);
    	overAllResult.setFeatureLines(featureLineSet);
    	overAllResultController.createOverAllResult(overAllResult);
    }
}
