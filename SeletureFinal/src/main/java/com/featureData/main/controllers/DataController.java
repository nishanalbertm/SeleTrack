package com.featureData.main.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.featureData.main.dao.DataDAO;
import com.featureData.main.entities.Data;


@RestController
@RequestMapping("/features")
public class DataController {
	
	@Autowired
	DataDAO dataDAO;
	
	/*Save*/
	@PostMapping("/inserttag")
	public Data createData(@Valid @RequestBody Data data){
		return dataDAO.save(data);
	}
	
	/*Retreive all data*/
	@GetMapping("/alltags")
	public List<Data> getAllData(){
		return dataDAO.findAll();	
	}
	
	/*Get a particular data*/
	@GetMapping("/tagid/{id}")
	public ResponseEntity<Data> getDataByName(@PathVariable(value="id") Long id){
		
		Data data = dataDAO.findOne(id);
		if(data==null){
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(data);
	}
	@GetMapping("/tagname/{tagname}")
	public ResponseEntity<Data> getDataByName(@PathVariable(value="tagname") String tagname){
		
		Data data = dataDAO.findOneByTagName(tagname);
		if(data==null){
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(data);
	}
	/*Update the data*/
	@PutMapping("/modifytagid/{id}")
	public ResponseEntity<Data> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody Data dataDetails){
		
		Data data = dataDAO.findOne(id);
		if(data==null){
			return ResponseEntity.notFound().build();
		}
		
			if(data.getCurrentcount()<dataDetails.getMaxcount()-1){
				data.setEnabled("Yes");
			}
			else{
				data.setEnabled("No");
			}
			
			data.setCurrentcount(dataDetails.getCurrentcount()+1);
			
			Data updateData = dataDAO.save(data);
			
		return ResponseEntity.ok().body(updateData);
		
	}
	
	/*Delete*/
	@DeleteMapping("/tagid/{id}")
	public ResponseEntity<Data> deleteData(@PathVariable(value="id") Long dataid){
		
		Data data = dataDAO.findOne(dataid);
		if(data==null){
			return ResponseEntity.notFound().build();
		}
		
		dataDAO.delete(data);
		return ResponseEntity.ok().build();
	}
	
}
