package com.featureData.main.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.featureData.main.dao.FeatureLineDAO;
import com.featureData.main.entities.FeatureLine;

@CrossOrigin
@RestController
@RequestMapping("/featurelines")
public class FeatureLineController {
	
	@Autowired
	FeatureLineDAO fLDAO;
	
	/*Save*/
	@PostMapping("/inserttag")
	public List<FeatureLine> createData(@Valid @RequestBody List<FeatureLine> fLList){
		return (List<FeatureLine>) fLDAO.save(fLList);
	}
	
	/*Retreive all data*/
	@GetMapping("/alltags")
	public List<FeatureLine> getAllData(){
		return fLDAO.findAll();	
	}
	
	/*Get a particular data*/
	@GetMapping("/tagid/{id}")
	public ResponseEntity<List<FeatureLine>> getDataByName(@PathVariable(value="id") Long id){
		
		List<FeatureLine> fL = fLDAO.findOne(id);
		if(fL==null){
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(fL);
	}
	
	
	@GetMapping("/instanceid/{instanceid}")
	public ResponseEntity<List<FeatureLine>> getDataByName(@PathVariable(value="instanceid") String instanceid){
		
		List<FeatureLine> fL= fLDAO.findByInstanceIDAndOrderByDoubleTypeStepNumber(instanceid);
		if(fL==null){
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(fL);
	}
	/*Update the data
	@PutMapping("/modifyfeaturelines/{id}")
	public ResponseEntity<Data> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody Data dataDetails){
		
		List<FeatureLine> fL = fLDAO.findOne(id);
		if(fL==null){
			return ResponseEntity.notFound().build();
		}
		
			fL.set
			data.setCurrentcount(dataDetails.getCurrentcount()+1);
			
			Data updateData = dataDAO.save(data);
			
		return ResponseEntity.ok().body(updateData);
		
	}
	
	/*Delete
	@DeleteMapping("/tagid/{id}")
	public ResponseEntity<Data> deleteData(@PathVariable(value="id") Long dataid){
		
		Data data = dataDAO.findOne(dataid);
		if(data==null){
			return ResponseEntity.notFound().build();
		}
		
		dataDAO.delete(data);
		return ResponseEntity.ok().build();
	}*/
	
}
