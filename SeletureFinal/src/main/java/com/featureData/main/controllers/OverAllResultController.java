package com.featureData.main.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.featureData.main.dao.FeatureLineDAO;
import com.featureData.main.dao.OverAllResultDAO;
import com.featureData.main.entities.FeatureLine;
import com.featureData.main.entities.OverAllResult;

@CrossOrigin
@RestController
@RequestMapping("/overallresults")
public class OverAllResultController {
	
	@Autowired
	OverAllResultDAO oraDAO;
	 
	@Autowired
	FeatureLineDAO fLDAO;
	
	/*Retreives all data*/
	@GetMapping(value = "/getall", produces =  "application/json", consumes =  "application/json")
	public List<OverAllResult> getAllOverAllResult(){
		List<OverAllResult> orderedList = oraDAO.findAll();
		for(OverAllResult oarResult: orderedList){
			List<FeatureLine> fL = fLDAO.findOne(oarResult.getTesesetid());
			oarResult.setFeatureLines(fL);
			
		}
		
		return orderedList;	
	}
	
	/*Save*/
	@PostMapping("/new")
	public OverAllResult createOverAllResult(@Valid @RequestBody OverAllResult ora){
		return oraDAO.save(ora);
	}
	
	/*Get a particular data*/
	@GetMapping("/getresultofid/{id}")
	public ResponseEntity<OverAllResult> getOverAllResultByid(@PathVariable(value="id") Long id){
			System.out.println("id :" + id);
			
		OverAllResult ora = oraDAO.findByTesesetid(id);
		System.out.println("First : " + ora);
				
		if(ora==null){
			
			return ResponseEntity.notFound().build();
		}
		List<FeatureLine> fL = fLDAO.findOne(ora.getTesesetid());
		ora.setFeatureLines(fL);
		
		System.out.println("Second - Sorted : " + ora);
		return ResponseEntity.ok().body(ora);
	}
	
	/*Update the data*/
	@PutMapping("/modifyoverallresultof/{id}")
	public ResponseEntity<OverAllResult> updateOverAllResult(@PathVariable(value="id") Long id, @Valid @RequestBody OverAllResult overAllResultDetails){
		
		OverAllResult ora = oraDAO.findOne(id);
		if(ora==null){
			return ResponseEntity.notFound().build();
		}
		
			//Unchecking the Run Option
			String c = "0";
		
			ora.setAccount(overAllResultDetails.getAccount());
			ora.setBrowser(overAllResultDetails.getBrowser());
			ora.setDependson(overAllResultDetails.getBrowser());
			ora.setPriority(overAllResultDetails.getPriority());
			ora.setRemarks(overAllResultDetails.getRemarks());
			ora.setStatus(overAllResultDetails.getStatus());
			ora.setRun(c);
			ora.setUserlevel(overAllResultDetails.getUserlevel());
			
		
			
			OverAllResult updateOverAllResult = oraDAO.save(ora);
			
		return ResponseEntity.ok().body(updateOverAllResult);
		
	}
	
	/*Delete*/
	@DeleteMapping("/deleteid/{id}")
	public ResponseEntity<OverAllResult> deleteOverAllResult(@PathVariable(value="id") Long oraid){
		
		OverAllResult ora = oraDAO.findOne(oraid);
		if(ora==null){
			return ResponseEntity.notFound().build();
		}
		
		oraDAO.delete(ora);
		return ResponseEntity.ok().build();
	}
	
}
