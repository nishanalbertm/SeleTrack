package com.featureData.main.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.featureData.main.entities.Data;
import com.featureData.main.repositories.DataRepository;

@Service
public class DataDAO {

	@Autowired
	DataRepository dataRepository;
	
	/*Save*/
	public Data save(Data data){
		return dataRepository.save(data);
	}
	
	/*Search*/
	public List<Data> findAll(){
		return dataRepository.findAll();
	}
	
	/*Get by ID*/
	public Data findOne(Long id){
		return dataRepository.findById(id).get();
	}
	
	/*Get by TagName whose count is least and enabled*/
	public Data findOneByTagName(String tagname){
		
		return dataRepository.findByTagNameWithLeastCountThatIsLessThanMaxCountAndAlsoEnabled(tagname);
	}
	
	
	/*Delete*/
	public void delete(Data data){
		dataRepository.delete(data);
	}
	
	
}
