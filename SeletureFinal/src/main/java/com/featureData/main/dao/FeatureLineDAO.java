package com.featureData.main.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.featureData.main.entities.FeatureLine;
import com.featureData.main.repositories.FeatureLineRepository;

@Service
public class FeatureLineDAO {

	@Autowired
	FeatureLineRepository featureLineRepository;
	
	/*Save*/
	public FeatureLine save(List<FeatureLine> fLList){
		
		
		return (FeatureLine) featureLineRepository.saveAll(fLList);
	}
	
	/*Search without Order*//*
	public List<FeatureLine> findAll(){
		return featureLineRepository.findAll();
	}*/
	
	/*Search By Ordering*/
	public List<FeatureLine> findAll(){
		List<FeatureLine> localList =  featureLineRepository.findAll();
		System.out.println(localList);
		return localList;
	}
	
	
	/*Get by ID*/
	public List<FeatureLine> findOne(Long id){
		return featureLineRepository.findByInstanceIDAndOrderByDoubleTypeStepNumber(id);

	}
	
	/*Get by TagName whose count is least and enabled*/

	
	
	/*Delete*/
	public void delete(FeatureLine fL){
		featureLineRepository.delete(fL);
	}

	public List<FeatureLine> findByInstanceIDAndOrderByDoubleTypeStepNumber(String instanceid) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
