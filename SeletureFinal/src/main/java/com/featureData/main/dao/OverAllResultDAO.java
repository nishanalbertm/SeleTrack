package com.featureData.main.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.featureData.main.entities.OverAllResult;
import com.featureData.main.repositories.OverAllResultRepository;

@Service
public class OverAllResultDAO {

	@Autowired
	OverAllResultRepository oarRepository;
	
	/*Search*/
	public List<OverAllResult> findAll(){
		List<OverAllResult> li = oarRepository.findAll();
		return li;
	}
	
	/*Save*/
	public OverAllResult save(OverAllResult ora){
		return oarRepository.save(ora);
	}
	
	/*Get by ID*/
	public OverAllResult findOne(Long id){
		return oarRepository.findById(id).get();
	}

	/*Delete*/
	public void delete(OverAllResult ora){
		oarRepository.delete(ora);
	}

	public OverAllResult findByTesesetid(Long id) {
		// TODO Auto-generated method stub
		return oarRepository.findById(id).get();
	}
}
