package com.featureData.main.entities;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name="Data")
@EntityListeners(AuditingEntityListener.class)
//@NamedQuery(name = "Data.findByTagNameWithLeastCountThatIsLessThanMaxCountAndAlsoEnabled", query ="SELECT * FROM `data` WHERE `tagname`= :arg1 and `enabled` = 'Yes' and `currentcount`<`maxcount` ORDER BY id ASC LIMIT 1")

public class Data {
	//primary key
	@Id
	//Auto-increment on insertion of new record
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@NotBlank
	private String tagname;
	
	private String description;
	
	@NotNull
	private Long maxcount;
	
	@NotNull
	private Long currentcount;
	
	@NotBlank
	private String enabled;
	
	private String username;
	private String password;
	private String data1;
	private String data2;
	private String data3;
	private String data4;
	private String data5;
	private String data6;
	private String data7;
	private String data8;
	private String data9;
	private String data10;
	private String data11;
	private String data12;
	private String data13;
	private String data14;
	private String data15;
	private String data16;
	private String data17;
	private String data18;
	private String data19;
	private String data20;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getData2() {
		return data2;
	}
	public void setData2(String data2) {
		this.data2 = data2;
	}
	public String getData3() {
		return data3;
	}
	public void setData3(String data3) {
		this.data3 = data3;
	}
	public String getData4() {
		return data4;
	}
	public void setData4(String data4) {
		this.data4 = data4;
	}
	public String getData5() {
		return data5;
	}
	public void setData5(String data5) {
		this.data5 = data5;
	}
	public String getData6() {
		return data6;
	}
	public void setData6(String data6) {
		this.data6 = data6;
	}
	public String getData7() {
		return data7;
	}
	public void setData7(String data7) {
		this.data7 = data7;
	}
	public String getData8() {
		return data8;
	}
	public void setData8(String data8) {
		this.data8 = data8;
	}
	public String getData9() {
		return data9;
	}
	public void setData9(String data9) {
		this.data9 = data9;
	}
	public String getData10() {
		return data10;
	}
	public void setData10(String data10) {
		this.data10 = data10;
	}
	public String getData11() {
		return data11;
	}
	public void setData11(String data11) {
		this.data11 = data11;
	}
	public String getData12() {
		return data12;
	}
	public void setData12(String data12) {
		this.data12 = data12;
	}
	public String getData13() {
		return data13;
	}
	public void setData13(String data13) {
		this.data13 = data13;
	}
	public String getData14() {
		return data14;
	}
	public void setData14(String data14) {
		this.data14 = data14;
	}
	public String getData15() {
		return data15;
	}
	public void setData15(String data15) {
		this.data15 = data15;
	}
	public String getData16() {
		return data16;
	}
	public void setData16(String data16) {
		this.data16 = data16;
	}
	public String getData17() {
		return data17;
	}
	public void setData17(String data17) {
		this.data17 = data17;
	}
	public String getData18() {
		return data18;
	}
	public void setData18(String data18) {
		this.data18 = data18;
	}
	public String getData19() {
		return data19;
	}
	public void setData19(String data19) {
		this.data19 = data19;
	}
	public String getData20() {
		return data20;
	}
	public void setData20(String data20) {
		this.data20 = data20;
	}

	
	
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date lastmodified;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTagname() {
		return tagname;
	}
	public void setTagname(String tagname) {
		this.tagname = tagname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getMaxcount() {
		return maxcount;
	}
	public void setMaxcount(Long maxcount) {
		this.maxcount = maxcount;
	}
	public Long getCurrentcount() {
		return currentcount;
	}
	public void setCurrentcount(Long currentcount) {
		this.currentcount = currentcount;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getData1() {
		return data1;
	}
	public void setData1(String data1) {
		this.data1 = data1;
	}
	
	public Date getLastmodified() {
		return lastmodified;
	}
	public void setLastmodified(Date lastmodified) {
		this.lastmodified = lastmodified;
	}
	
	public String toString() {
		String jsonString = "";
        try {
            final ObjectMapper mapper = new ObjectMapper();
            jsonString = mapper.writeValueAsString(this);          
        }
        catch (final IOException e) {
        	System.out.println(String.format("Exception occured while converting object to json : %s ", jsonString));
        }
        return jsonString;
	}

}
