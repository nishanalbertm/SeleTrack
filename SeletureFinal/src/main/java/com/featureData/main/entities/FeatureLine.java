package com.featureData.main.entities;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name="testinstanceresult")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class FeatureLine implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="featureLine_seq")
	@SequenceGenerator(name="featureLine_seq", sequenceName="featureLine_seq", allocationSize=1)
	private Long id;
	
	private String stepnumber;
	
	private String actual_result;
	
	private String status;
	
	private String description;
	
	@NotNull
	private String featureline;

	private Date begintime;
	
	private Date endtime;
	
	private Date executiontime;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="instance_id", referencedColumnName="tesesetid")
	@JsonBackReference
	private OverAllResult overAllResult;
	
	public FeatureLine() {
		super();
	}

	public FeatureLine(String stepnumber, String actual_result, String status, String description,
			@NotNull String featureline, Date begintime, Date endtime, Date executiontime, OverAllResult overAllResult) {
		super();
		this.stepnumber = stepnumber;
		this.actual_result = actual_result;
		this.status = status;
		this.description = description;
		this.featureline = featureline;
		this.begintime = begintime;
		this.endtime = endtime;
		this.executiontime = executiontime;
		this.overAllResult = overAllResult;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStepnumber() {
		return stepnumber;
	}

	public void setStepnumber(String stepnumber) {
		this.stepnumber = stepnumber;
	}
	
	public String getActual_result() {
		return actual_result;
	}

	public void setActual_result(String actual_result) {
		this.actual_result = actual_result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFeatureline() {
		return featureline;
	}

	public void setFeatureline(String featureline) {
		this.featureline = featureline;
	}

	public Date getBegintime() {
		return begintime;
	}

	public void setBegintime(Date begintime) {
		this.begintime = begintime;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	public Date getExecutiontime() {
		return executiontime;
	}

	public void setExecutiontime(Date executiontime) {
		this.executiontime = executiontime;
	}

	public OverAllResult getOverAllResult() {
		return overAllResult;
	}

	public void setOverAllResult(OverAllResult overAllResult) {
		this.overAllResult = overAllResult;
	}

	public String toString() {
		String jsonString = "";
        try {
            final ObjectMapper mapper = new ObjectMapper();
            jsonString = mapper.writeValueAsString(this);          
        }
        catch (final IOException e) {
        	System.out.println(String.format("Exception occured while converting object to json : %s ", jsonString));
        }
        return jsonString;
	}
}
