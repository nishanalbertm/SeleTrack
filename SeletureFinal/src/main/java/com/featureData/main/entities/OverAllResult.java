package com.featureData.main.entities;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name="testsetauto")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class OverAllResult implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="OverAllResult_seq")
	@SequenceGenerator(name="OverAllResult_seq", sequenceName="OverAllResult_seq", allocationSize=1)
	private Long tsetaid;
	
	private Long tesesetid;
	
	private String run;
	
	private String status;
	
	private String remarks;
	
	private String dependson;
	
	private Integer priority;

	private String browser;
	
	private String account;
	
	private String userlevel;
	
	@OneToMany(targetEntity=FeatureLine.class, mappedBy="overAllResult", cascade=CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval = true)
	@JsonManagedReference
	private List<FeatureLine> featureLines;
	
	public Long getTsetaid() {
		return tsetaid;
	}

	public void setTsetaid(Long tsetaid) {
		this.tsetaid = tsetaid;
	}

	public Long getTesesetid() {
		return tesesetid;
	}

	public void setTesesetid(Long tesesetid) {
		this.tesesetid = tesesetid;
	}

	public String getRun() {
		return run;
	}

	public void setRun(String run) {
		this.run = run;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDependson() {
		return dependson;
	}

	public void setDependson(String dependson) {
		this.dependson = dependson;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getUserlevel() {
		return userlevel;
	}

	public void setUserlevel(String userlevel) {
		this.userlevel = userlevel;
	}

	public List<FeatureLine> getFeatureLines() {
		return featureLines;
	}

	public void setFeatureLines(List<FeatureLine> featureLines) {
		this.featureLines = featureLines;
	}

	public OverAllResult() {
		super();
	}

	public OverAllResult(Long tesesetid, String run, String status, String remarks, String dependson, Integer priority,
			String browser, String account, String userlevel, List<FeatureLine> featureLines) {
		super();
		this.tesesetid = tesesetid;
		this.run = run;
		this.status = status;
		this.remarks = remarks;
		this.dependson = dependson;
		this.priority = priority;
		this.browser = browser;
		this.account = account;
		this.userlevel = userlevel;
		this.featureLines = featureLines;
	}

	public String toString() {
		String jsonString = "";
        try {
            final ObjectMapper mapper = new ObjectMapper();
            jsonString = mapper.writeValueAsString(this);          
        }
        catch (final IOException e) {
        	System.out.println(String.format("Exception occured while converting object to json : %s ", jsonString));
        }
        return jsonString;
	}
}
