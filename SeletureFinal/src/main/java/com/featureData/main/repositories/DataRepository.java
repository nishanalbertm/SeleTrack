package com.featureData.main.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import com.featureData.main.entities.Data;

//Note this is an INTERFACE
public interface DataRepository extends JpaRepository<Data, Long>{
	
	@Query(value = "SELECT * FROM `data` WHERE `tagname`= ?1 and `enabled` = 'Yes' and `currentcount`<`maxcount` ORDER BY `currentcount` ASC LIMIT 1" , nativeQuery = true)
	Data findByTagNameWithLeastCountThatIsLessThanMaxCountAndAlsoEnabled(String tagname);
	
}
