package com.featureData.main.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import com.featureData.main.entities.FeatureLine;

//Note this is an INTERFACE
public interface FeatureLineRepository extends JpaRepository<FeatureLine, Long>{
	
	@Query(value = "SELECT * FROM `testinstanceresult` WHERE `instance_id`=?1 ORDER BY `stepnumber` ASC" , nativeQuery = true)
	List<FeatureLine> findByInstanceIDAndOrderByDoubleTypeStepNumber(Long instanceid);
	
}
