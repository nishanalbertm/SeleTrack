package com.featureData.main.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.featureData.main.entities.OverAllResult;


//Note this is an INTERFACE
public interface OverAllResultRepository extends JpaRepository<OverAllResult, Long>{

	Optional<OverAllResult> findByTesesetid(Long id);

}
